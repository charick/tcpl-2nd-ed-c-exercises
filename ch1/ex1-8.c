/* chapter 1, ex 1-8: write a program to count blanks, tabs and newlines. */
#include <stdio.h>

#define NEWLINE '\n'
#define TAB '	'
#define BLANK ' '

main()
{
	int c, tabs, blanks, newlines;

	tabs = 0;
	blanks = 0;
	newlines = 0;

	while((c = getchar()) != EOF)
	{
		if(c == TAB)
			++tabs;
		else if (c == BLANK)
			++blanks;
		else if (c == NEWLINE)
			++newlines;
	}
	printf("tabs: %3d || blanks: %3d || newlines: %3d\n", tabs, blanks, newlines);
}
