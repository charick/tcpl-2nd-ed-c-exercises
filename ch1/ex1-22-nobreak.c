#include <stdio.h>

#define MAX_COLUMNS 12 /* The column at which we would begin "folding" long lines */
#define COLUMN_WIDTH 8 /* the width of each of our columns, aka tab spacing. */
#define MAX_LINE 1000 /* the max length of a line, in general. */

/* Write a program to "fold" long input lines into two or more shorter lines after the last
 * non-blank character that occurs before the n-th column of input. Make sure your program
 * does something intelligent with very long lines, and if there are no blanks or tabs before
 * the specified column. */

void print(int str[]);

int main()
{
	const int max_len = MAX_COLUMNS * COLUMN_WIDTH;
	int c,i,len;
	int linebuffer[MAX_LINE];

	len = 0; 

	for(i = 0; (c = getchar()) != EOF; ++i)
	{
		linebuffer[i] = c;
		if ( c == '\t' )
			len += (COLUMN_WIDTH - (len % COLUMN_WIDTH));	
		else if ( c == '\n' )
		{
			len = 0;
			linebuffer[++i] = '\0';
			print(linebuffer);
			i = -1;
		}
		else
			++len;

		if ( len >= max_len )
		{
			int templen = len;
			int start;
			int copybuffer[MAX_LINE];

			for	(start = i; 
					templen + 2 >= max_len 
					|| (linebuffer[start] != ' ' && linebuffer[start] != '\t');
				--start)
			{
				if ( linebuffer[start] == '\t' )
					templen -= (COLUMN_WIDTH - (templen % COLUMN_WIDTH));
				else
					--templen;
			}
			++start;

			int j;

			for(j = 0; j < (i - start); ++j)
				copybuffer[j] = linebuffer[start + j];

			linebuffer[start] = '\n';
			linebuffer[++start] = '\0';

			print(linebuffer);
			i = j;
			len = 0;
			
			for(int k = 0; k < j; ++k)
				linebuffer[k] = copybuffer[k];
			linebuffer[i] = c;
		}
	}
}

void print(int str[])
{
	for(int i = 0; str[i] != '\0'; ++i)
		putchar(str[i]);
}
