#include <stdio.h>

#define IN 1 /* inside a word */
#define OUT 0 /* outside a word */

/* write a program that prints its input one word per line. */
int main()
{
	int c, state;
	state = OUT;

	while((c = getchar()) != EOF)
	{
		if( c == '\n' )
			; /* ignore newlines as they are irrelevant to the program */
		if( c == ' ' || c == '\t' )
		{
			if( state == IN )
				putchar('\n');
			state = OUT;
		} else {
			putchar(c);
			state = IN;
		}
	}
}
