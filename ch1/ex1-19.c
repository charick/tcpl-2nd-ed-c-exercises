#include <stdio.h>

#define MAX_LEN 1000 /* the maximum supported line length */

/* Write a function reverse(s) that reverses the character string s. Use it to write
 * a program that reverses its input a line at a time. */
void reverse(int string[], int reversed[], int len);
void print(int string[]);
void printreversed(int string[], int len);

int main()
{
	int c, len;
	int linebuffer[MAX_LEN];

	for(len = 0; len < MAX_LEN && (c = getchar()) != EOF; len++)
	{
		linebuffer[len] = c;

		if ( c == '\n' )
		{
			printreversed(linebuffer, len);
			len = -1; /* reset to -1 because var is incremented end-of-cycle */
		}
	}

	/* Handles a line passed with no \n */
	if ( len > 0 )
	{
		printreversed(linebuffer, len);
		putchar('\n');
	}
}

void printreversed(int string[], int len)
{
	int reversed[len + 1];
	reverse(string, reversed, len);
	reversed[len + 1] = '\0';
	print(reversed);
}

void reverse(int string[], int reversed[], int len)
{
	for(int i = len; i >= 0; i--)
		reversed[len - i] = string[i];
}

void print(int string[])
{
	for(int i = 0; string[i] != '\0'; i++)
		putchar(string[i]);
}
