#include <stdio.h>

#define PRINT_THRESHOLD 80 /* the length at which we decide the line is long enough and print it */
#define MAX_LENGTH 1000 /* the max length of a line which we wish to record */

/* Write a program to print all input lines that are longer than 80 characters */

void print(int string[]);

int main()
{
	int c, len;
	int curline[MAX_LENGTH];

	for(len = 0; (c = getchar()) != EOF && len < MAX_LENGTH - 1; len++)
	{
		curline[len] = c;

		if ( c == '\n' )
		{
			curline[++len] = '\0';
			if ( len >= PRINT_THRESHOLD )
				print(curline);
			len = -1; /* set to -1 because it is incremented by one for the next loop iter */
		}
	}
}

void print(int string[])
{
	int c = 0;
	for(int i = 0; (c = string[i]) != '\0'; i++)
		putchar(c);
}
