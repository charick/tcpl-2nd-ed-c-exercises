#include <stdio.h>

#define MAX_COLUMNS 12 /* The column at which we would begin "folding" long lines */
#define COLUMN_WIDTH 8 /* the width of each of our columns, aka tab spacing. */
#define MAX_LINE 1000 /* the max length of a line, in general. */

/* Write a program to "fold" long input lines into two or more shorter lines after the last
 * non-blank character that occurs before the n-th column of input. Make sure your program
 * does something intelligent with very long lines, and if there are no blanks or tabs before
 * the specified column. */

void print(int str[]);
int main()
{
	const int max_len = MAX_COLUMNS * COLUMN_WIDTH;
	int c, i, len;
	int linebuffer[MAX_LINE];

	for(i = 0, len = 0; (c = getchar()) != EOF; ++i)
	{
		linebuffer[i] = c;
		if ( c == '\t' )
			len += (COLUMN_WIDTH - (len % COLUMN_WIDTH));
		else if ( c == '\n' )
		{
			linebuffer[++i] = '\0';
			print(linebuffer);
			len = 0;
			i = -1;
		}
		else
			++len;

		if ( len >= max_len )
		{
			if ( c != ' ' && c != '\t' )
			{
				linebuffer[i] = '-';
				linebuffer[++i] = '\n';
				linebuffer[++i] = '\0';
				print(linebuffer);
				linebuffer[0] = c; 
				i = 0;
				len = 1;
			} else {
				linebuffer[i] = '\n';
				linebuffer[++i] = '\0';
				print(linebuffer);
				i = -1;
				len = 0;
			}
		}
	}
}

void print(int str[])
{
	for(int i = 0; str[i] != '\0'; ++i)
		putchar(str[i]);
}
