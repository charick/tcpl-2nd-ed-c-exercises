/* Write a program that copies input -> output, replaces tab by \t, backspace by \b and backslash by \\ */
#include <stdio.h>

#define TAB '\t'
#define BACKSPACE '\b'
#define BACKSLASH '\\'

main()
{
	int c;

	while((c = getchar()) != EOF)
	{
		if( c == TAB ) {
			putchar('\\');
			putchar('t');
		} else if ( c == BACKSPACE ) {
			putchar('\\');
			putchar('b');
		} else if ( c == BACKSLASH ) {
			putchar('\\');
			putchar('\\');
		} else putchar(c);
	}
}
