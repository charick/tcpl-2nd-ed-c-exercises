#include <stdio.h>

#define ASCII_CHARS 128 /* 128 chars in ASCII charset */

/* Write a program to print a histogram of the frequencies of different characters in its input */

int main() 
{
	int c;
	int char_frequencies[ASCII_CHARS]; /* ASCII has 128 chars, so this is only ASCII-compatible. */
	
	for(int i = 0; i < ASCII_CHARS; i++)
		char_frequencies[i] = 0; /* initialize array values to 0 */

	while((c = getchar()) != EOF)
	{
		if(c <= 127) /* safety check incase non-ascii chars are passed in. */
			++char_frequencies[c];
	}

	for(int i = 0; i < ASCII_CHARS; i++)
	{
		if(char_frequencies[i] > 0 && i != ' ')
		{
			if(i == '	')
				printf(" \\t || ");
			else if (i == '\n')
				printf(" \\n || ");
			else 
				printf("%3c || ", i);

			for(int j = 0; j < char_frequencies[i]; j++)
				putchar('#');
			putchar('\n');
		}
	}
	printf("\nSpaces: %d\n", char_frequencies[' ']);
}
