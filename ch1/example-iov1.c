/* copy input to output; 1st version */
#include <stdio.h>
main() 
{
	int c; /* we use an int here because c must be big enough to hold ANY char PLUS the EOF special char */


	c = getchar();
	while(c != EOF)
	{
		putchar(c);
		c = getchar();
	}
}
