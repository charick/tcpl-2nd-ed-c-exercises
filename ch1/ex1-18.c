#include <stdio.h>

#define IN 1 /* in a series of blanks or tabs */
#define OUT 0 /* not in a series of blanks or tabs */
#define MAX_LEN 1000 /* the max size of a line we wish to support */

/* Write a program to remove trailing ' ' and tabs from each line, and to delete entirely blank lines */

void print(int string[]);

int main()
{
	int i, c, state;
	int start; /* the start index of our trailing ' ' and '\t' */
	int linebuffer[MAX_LEN];

	state = OUT;
	start = 0;

	for(i = 0; i < MAX_LEN && (c = getchar()) != EOF; i++)
	{
		linebuffer[i] = c;

		if ( state == IN && c == '\n' && start > 0 )
		{
			linebuffer[start] = c;
			i = start;	
			start = 0;
		} 
		else if ( state == OUT && ( c == ' ' || c == '\t' ) )
		{
			state = IN;
			start = i;
		}
		else if ( state == IN && c != ' ' && c != '\t' )
		{
			state = OUT;
			start = 0;
		}

		if ( c == '\n' )	
		{
			if ( i > 0 )
			{
				linebuffer[i + 1] = '\0';
				print(linebuffer);
			}
			state = OUT;
			i = -1;
			start = 0;
		}
	}
}

void print(int string[])
{
	int c;
	for(int i = 0; (c = string[i]) != '\0'; i++)
		putchar(c);
}
