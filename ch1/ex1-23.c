#include <stdio.h>

#define MAX_LINE 1000 /* the max length of our lines */
#define IN 1 /* we are inside a comment */ 
#define OUT 0 /* we are outside a comment */


/* Write a program to remove all comments from a C program. Don't forget to handle quoted strings
 * and character constants properly. C comments do not nest. */

void print(int str[]);

int main()
{
	int c, i, s;
	int flags[2]; /* this will hold our truthy values for the presence of \* and \/. */
	int linebuffer[MAX_LINE];

	s = OUT;
	
	for(i = 0; (c = getchar()) != EOF; ++i)
	{
		if ( c == '*' )
			flags[0] = 1;
		else if ( c == '/' )
			flags[1] = 1;
		else
			flags[0] = flags[1] = 0;

		if ( s == OUT && flags[0] && flags[1] )
		{
			s = IN;
			flags[0] = flags[1] = 0;
			i -= 2;
		} 
		else if ( s == IN && flags[0] && flags[1] )
		{
			s = OUT;
			flags[0] = flags[1] = 0;
			--i;
		}
		else if ( s == OUT )
			linebuffer[i] = c;
		else
			--i;

		if ( c == '\n' )
		{
			linebuffer[++i] = '\0';
			print(linebuffer);
			i = -1;
		}
	}

	if ( i > 0 )
	{
		linebuffer[++i] = '\0';
		print(linebuffer);
	}
}

void print(int str[])
{
	for(int i = 0; str[i] != '\0'; ++i)
		putchar(str[i]);
}
