/* Revised conversion program using constants and more streamlined code. */
#include <stdio.h>

/* constants! */
#define LOWER 0 /* lower limit of the table */
#define UPPER 300 /* upper limit of the table */
#define STEP 20 /* the per-cycle increment of the to-be-converted value */

main() 
{
	int fahr;

	for (fahr = LOWER; fahr <= UPPER; fahr = fahr + STEP)
		printf("%3d %6.1f\n", fahr, (5.0/9.0)*(fahr-32));
}
