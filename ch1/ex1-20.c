#include <stdio.h>

#define COLUMN_WIDTH 8 /* the width of each column, aka the spacing of tab stops. */

/* Write a program that replaces tabs in the input with the proper number of blanks to space to the next
 * tab stop. (note: tab stop = every 8th column. 12345678|12345678|) */

void fillblanks(int count);

int main()
{
	int c, i, cpos, target;
	
	cpos = 0; /* cpos is our current pos in the column, 0-COLUMN_WIDTH */

	for(i = 0; (c = getchar()) != EOF; ++i)
	{
		cpos = i % COLUMN_WIDTH;
		if ( c == '\t' )
		{
			fillblanks(COLUMN_WIDTH - cpos);
			--i;
			/* decrement i because we are filling t-c, including the \t char. */
		}
		else
			putchar(c);

		if ( c == '\n' )
			i = -1; /* reset our index to 0 on newlines. */
	}
}

void fillblanks(int count)
{
	for(; count > 0; count--)
		putchar(' ');	
}
