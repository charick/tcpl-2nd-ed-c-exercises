/* Write a program to copy its input to its output, replacing each string of one or more blanks by a single blank */
#include <stdio.h>

#define BLANK ' '
#define IN_BLANKS 1
#define OUT_BLANKS 0

main()
{
	int c, state;
	
	state = OUT_BLANKS;

	while((c = getchar()) != EOF)
	{
		if( c != BLANK && state == IN_BLANKS )
		{
			putchar(BLANK);
			putchar(c);
			state = OUT_BLANKS;
		}
		else if ( c == BLANK )
			state = IN_BLANKS;
		else putchar(c);
	}
}
