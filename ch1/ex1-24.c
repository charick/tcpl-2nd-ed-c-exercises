#include <stdio.h>

#define OUT 0 /* we are not currently in a string or comment */
#define IN_C 1 /* we are currently in a comment */
#define IN_S 2  /* we are currently in a string */

/* Write a program to check a C program for rudimentary syntax errors like unbalanced parenthesis,
 * brackets and braces. Don't forget about quotes, both single and double, escape sequences,
 * and comments. */

/* NOTE: I abstained from using a switch here because it has not been introduced by the book
 * yet, implying that the book intended this problem to be solved *without* a switch.
 */

int main()
{
	int i,c,s,l,p,sc,is_escape;
	int flags[3];
	int lines[3];
	int lc[3];/* the column at which the bracket/paren/brace occurred */

	is_escape = 0; /* whether or not we are in an escape sequence */
	s = OUT;
	l = 1; /* We start on line 1. */
	p = '\0'; /* the previously-read char */
	sc = '\0'; /* the char that initiated the string we are inside. */

	for(i = 0; (c = getchar()) != EOF; ++i)
	{
		is_escape = p == '\\' && c != '\\';

		if ( s == OUT && !is_escape )
		{
			if ( c == '{' )
			{
				++flags[0];
				lines[0] = l;
				lc[0] = i;
			}
			else if ( c == '}' )
			{
				--flags[0];
			}
			else if ( c == '(' )
			{
				++flags[1];
				lines[1] = l;
				lc[1] = i;
			}
			else if ( c == ')' )
			{
				--flags[1];
			}
			else if ( c == '[' )
			{
				++flags[2];
				lines[2] = l;
				lc[2] = i;
			}
			else if ( c == ']' )
			{
				--flags[2];
			}
			else if ( c == '\'' || c == '"' )
			{
				s = IN_S;
				sc = c;
			}
			else if ( c == '*' && p == '/' )
			{
				s = IN_C;
			}
		}
		else if ( s == IN_S && !is_escape )
		{
			if ( c == sc )
				s = OUT;
		}
		else if ( s == IN_C && !is_escape )
		{
			if ( c == '/' && p == '*' )
				s = OUT;
		}

		if ( c == '\n' )
		{
			++l;
			i = -1;
		}

		p = c;
	}

	if ( flags[0] )
		printf("Unmatched brace ('{') at %d:%d\n", lines[0], lc[0]);
	if ( flags[1] )
		printf("Unmatched parenthesis ('(') at %d:%d\n", lines[1], lc[1]);
	if ( flags[2] )
		printf("Unmatched brackets ('[') at %d:%d\n", lines[2], lc[2]);
	if ( s == IN_S )
		printf("Unterminated string: expected %c\n", sc);
	else if ( s == IN_C )
		printf("Unterminated comment: expected '*/'\n");

	if ( !flags[0] && !flags[1] && !flags[2] && s == OUT )
		printf("No syntax errors detected :) \n");
}
