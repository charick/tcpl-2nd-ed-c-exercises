#include <stdio.h>

#define IN 1 /* inside a word */
#define OUT 0 /* outside a word */
#define MAX_LEN 100 /* the maximum supported length */

/* Write a program to print a histogram of the lengths of words in its input. (Horizontal) */
int main()
{
	int c,state,wlen;
	int lncounts[MAX_LEN]; /* Support words up to 100 chars in length... */
	
	for(int i = 0; i <= MAX_LEN; i++)
		lncounts[i] = 0; /* initialize array values to 0 */

	state = OUT;
	wlen = 0;

	while((c = getchar()) != EOF) 
	{
		if( state == IN )
		{
			if ( c == ' ' || c == '\t' || c == '\n' )
			{
				state = OUT;
				if( wlen <= MAX_LEN )
					++lncounts[wlen];
				wlen = 0;
			}
			else
				++wlen;
		}
		else if ( c != ' ' && c != '\t' && c != '\n' )
		{
			state = IN;
			++wlen;
		}
	}

	for(int i = 0; i <= MAX_LEN; i++)
	{
		if( lncounts[i] > 0 ) /* skip counts of 0 */
		{
			printf("%3d || ", i);
			for(int j = 0; j < lncounts[i]; j++)
				putchar('#');
			putchar('\n');
		}
	}
}
