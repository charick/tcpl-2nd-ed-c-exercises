/* ch1, ex 1-4: write a program to print the celcius to fahrenheight conversion table */
#include <stdio.h>
main()
{
	float fahr, celcius;
	int lower, upper, step;

	lower = 0;
	upper = 300;
	step = 20;

	celcius = lower;
	while(celcius <= upper)
	{
		fahr = (celcius * (9.0/5.0)) + 32;
		printf("%3.0f %6.1f\n", celcius, fahr);
		celcius = celcius + step;
	}
}

