#include <stdio.h>

#define COLUMN_WIDTH 8 /* the width of our columns, which determines the spacing of tabs. */
#define MAX_LINE 1000 /* the maximum length of a line */

/* Write a program that replaces strings of blanks by the minimum number of tabs and blanks
 * to achieve the same spacing. */

void print(int string[]);

int main() 
{
	const int c, i, start, blanks;
	bool j;
	int linebuffer[MAX_LINE];
	start = -1;
	blanks = 0;

	for(i = 0; (c = getchar()) != EOF; ++i)
	{
		linebuffer[i] = c;
		if ( c == ' ' || c == '\t' )
		{
			if ( c == ' ' )
				++blanks;
			else
				blanks += (COLUMN_WIDTH - (blanks % COLUMN_WIDTH));
			if ( start == -1 )
				start = i;
		}
		else
		{
			if ( blanks >= COLUMN_WIDTH )
			{
				int num_tabs = blanks / COLUMN_WIDTH;
				int num_blanks = blanks % COLUMN_WIDTH;
				int end = start + num_tabs;
				int origin = i;

				for(i = start; i < end; ++i)
					linebuffer[i] = '\t';
				end = i + num_blanks;
				for(; i < end; ++i)
					linebuffer[i] = ' ';
				if ( origin != start + num_tabs + num_blanks )
					linebuffer[i] = c;
			}

			if ( c == '\n' )
			{
				linebuffer[++i] = '\0';
				print(linebuffer);
				i = -1;
			}
			start = -1;
			blanks = 0;
		}
	}
	
	if ( i > 0 )
	{
		linebuffer[++i] = '\0';
		print(linebuffer);
	}
}

void print(int string[])
{
	for(int i = 0; string[i] != '\0'; i++)
		putchar(string[i]);
}
