#include <stdio.h>

/* print Fahrenheight-Celsius table for fahr = 0, 20, ..., 300 */
/* ch1, ex 1-5: modify the program so that it prints the table in reverse order */
/* ch1, ex 1-15: rewrite the temperature conversion program to use a function for conversion */
/* also includes some other changes at this stage, like int main() and return 0; */

float convert(int fahr);

int main()
{
	float fahr, celsius;
	int lower, upper, step;

	/* print heading above table, ch1 ex 1-3 */
	printf("||Fahrenheight->Celcius Conversions||\n");

	lower = 0; /* lower limit of temperature table */
	upper = 300; /* upper limit */
	step = 20; /* step size */

	fahr = upper;
	while (fahr >= lower) {
		celsius = convert(fahr);
		printf("%3.0f %6.1f\n", fahr, celsius); /* print value 3 chars wide with 0 decimal places, 6 chars wide with 1 decimal place. */ 
		fahr = fahr - step;
	}
	return 0;
}

float convert(int fahr)
{
	return (5.0/9.0) * (fahr - 32.0);
}
/* other things to note about printf: %.2f specifies no width constraint, 2 decimal places.
 * %d - decimal int
 * %6d - decimal int, minimum 6 chars wide (fills extra space with blanks)
 * %o - octal
 * %x - hexadecimal
 * %c - char
 * %s - string
 * %% - % itself */
